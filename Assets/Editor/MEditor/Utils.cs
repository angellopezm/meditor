﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Utils {
    public static void Save(string path, object obj) {
        if (File.Exists(path)) {
            File.Delete(path);
        }
        using (FileStream fs = new FileStream(path, FileMode.Create)) {
            using (StreamWriter writer = new StreamWriter(fs)) {
                writer.Write(JsonUtility.ToJson(obj));
            }
        }
    }

    public static T Load<T>(string path) {
        T result = default(T);
        if (File.Exists(path)) {
            using (FileStream fs = new FileStream(path, FileMode.Open)) {
                using (StreamReader reader = new StreamReader(fs)) {
                    result = JsonUtility.FromJson<T>(reader.ReadToEnd());
                }
            }
        }
        return result;
    }
}
