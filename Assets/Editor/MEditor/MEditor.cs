﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEditor;
using System;
using System.IO;
using System.Reflection;
using System.Linq;
using UnityEngine.UIElements;

public class MEditor: EditorWindow {

    private static readonly Lazy<MEditor> lazy = new Lazy<MEditor>(() => GetWindow<MEditor>());
    VisualTreeAsset uiSchema;
    StyleSheet uiStyle;
    Vector2 scrollPos;
    int tab;
    List<string> sections;

    public static MEditor Instance { get { return lazy.Value; } }

    private MEditor() {
    }

    public List<AbstractMEditorModule> modules;

    [MenuItem("Tools/MEditor")]
    private static void Init() {

        Instance.Show();
        //Instance.Close(); //IF WINDOW BUGS

    }

    private void OnEnable() {

        sections = new List<string>();

        // ADD MODULES HERE
        modules = new List<AbstractMEditorModule>() {
            MEGrid.Instance,
            MERoom.Instance,
            MESceneManager.Instance,
            METilesetLoader.Instance,
            METilePainter.Instance,
            MECollisionEditor.Instance,

        };

        foreach (AbstractMEditorModule module in modules) {
            if (!sections.Contains(module.GetType().GetCustomAttribute<MenuTab>().Section) && module.GetType().GetCustomAttribute<MenuTab>().Section != "General") {
                sections.Add(module.GetType().GetCustomAttribute<MenuTab>().Section);
            }
        }

    }

    private void OnInspectorUpdate() {

        try
        {
            minSize = new Vector2(Screen.width/4, minSize.y);
        }
        catch (NullReferenceException) { }

        Resources.UnloadUnusedAssets();
        GC.Collect();

        Repaint();
        SceneView.RepaintAll();

        //roomModule.gridprops = gridModule.props;

        // Delete unused Editor's instances

        Editor[] remainingBuggedEditors = FindObjectsOfType<Editor>();


        //Debug.Log(remainingBuggedEditors.Length);

        foreach (var editor in remainingBuggedEditors) {
            if (editor != this) {
                DestroyImmediate(editor);
            }
        }

    }    

    // All the Menu GUI Methods to render
    private void OnGUI() {

        tab = GUILayout.Toolbar(tab, sections.ToArray());

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
        switch (sections[tab]) {
            case "Collision":
                GUILayout.BeginVertical();

                foreach (AbstractMEditorModule module in modules.Where(x => x.GetType().GetCustomAttribute<MenuTab>().Section == "Collision")) {
                    module.OnMenu();
                }

                GUILayout.EndVertical();
                break;

            case "Tiling":
                GUILayout.BeginVertical();

                foreach (AbstractMEditorModule module in modules.Where(x => x.GetType().GetCustomAttribute<MenuTab>().Section == "Tiling")) {
                    module.OnMenu();
                }

                GUILayout.EndVertical();
                break;
        }

        foreach (AbstractMEditorModule module in modules.Where(x => x.GetType().GetCustomAttribute<MenuTab>().Section == "General")) {
            module.OnMenu();
        }

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.EndScrollView();

    }
}
    