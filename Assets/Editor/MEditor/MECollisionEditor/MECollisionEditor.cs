﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[MenuTab(Section = "Collision")]
public class MECollisionEditor : AbstractMEditorModule {

    private static readonly Lazy<MECollisionEditor> lazy = new Lazy<MECollisionEditor>(() => CreateInstance<MECollisionEditor>());
    public MECollisionEditorProps props;
    public MERoomProps roomprops = MERoomProps.Instance;
    public MEGridProps gridprops = MEGridProps.Instance;
    public string path;

    public static MECollisionEditor Instance { get { return lazy.Value; } }

    private MECollisionEditor() {
    }

    private void OnEnable() {
        path = Application.dataPath + "/Editor/MEditor/" + GetType().Name + "/conf.json";

        if (File.Exists(path)) {
            MECollisionEditorProps.Instance = Utils.Load<MECollisionEditorProps>(path);
        }
        props = MECollisionEditorProps.Instance;

        SceneView.duringSceneGui -= OnSceneGUI;
        SceneView.duringSceneGui += OnSceneGUI;
    }

    override public void OnSceneGUI(SceneView sView) {
        //Render();

        //Tools.current = Tool.None; // No se puede usar herramienta de Unity
        /*
        //--------------------------------------------------------------
        MECore.myE = Event.current;
        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

        if ((MECore.myE.type == EventType.MouseDrag || MECore.myE.type == EventType.MouseDown)) {
            MECore.mousePos = Event.current.mousePosition;
            MECore.mousePos.y = SceneView.currentDrawingSceneView.camera.pixelHeight - MECore.mousePos.y;
            MECore.mouseWorldPos = SceneView.currentDrawingSceneView.camera.ScreenPointToRay(MECore.mousePos).origin;

            if (gridprops.x > 0.0f && gridprops.y > 0.0f) {
                mouseWorldPos.x = Mathf.Floor(mouseWorldPos.x / MEGrid.gridSize.x);
                mouseWorldPos.y = Mathf.Ceil(mouseWorldPos.y / MEGrid.gridSize.y);

            }

            if (MECore.myE.button == 0) {
                try { MECore.collisionArray[Mathf.RoundToInt(MECore.mouseWorldPos.x), MECore.collisionArray.GetLength(1) - Mathf.RoundToInt(MECore.mouseWorldPos.y), MECore.zDim] = MECore.pickedValue; } catch (IndexOutOfRangeException) { }
            }

            if (MECore.myE.shift && MECore.myE.button == 0) {
                try { MECore.collisionArray[Mathf.RoundToInt(MECore.mouseWorldPos.x), MECore.collisionArray.GetLength(1) - Mathf.RoundToInt(MECore.mouseWorldPos.y), MECore.zDim] = 0; } catch (IndexOutOfRangeException) { }
            }
        }

        */
    }

    private void Render() {
        GUIStyle style = new GUIStyle();
        style.fontSize = 10;
        //--------------------------------------------------------------
        try {
            Vector3 camPos = SceneView.currentDrawingSceneView.camera.ScreenPointToRay(new Vector3(0f, 0f, 0f)).origin;
            Vector3 camDim = SceneView.currentDrawingSceneView.camera.ScreenPointToRay(new Vector3(SceneView.currentDrawingSceneView.camera.pixelWidth, SceneView.currentDrawingSceneView.camera.pixelHeight)).origin;
            for (int i = 0; i < props.box.GetLength(0); i++) {
                for (int j = 0; j < props.box.GetLength(1); j++) {
                    if (props.box[i][j][(int) roomprops.dim.z] == 0) style.normal.textColor = new Color32(242, 242, 242, 255);
                    else if (props.box[i][j][(int) roomprops.dim.z] % 2 == 0) style.normal.textColor = new Color32(255, 67, 0, 255);
                    else { style.normal.textColor = new Color32(255, 178, 0, 255); }
                    Vector3 placeToDraw = new Vector3(i * gridprops.size.x + (gridprops.size.x / 2), roomprops.dim.y - (j * gridprops.size.y + (gridprops.size.y / 2.5f)), roomprops.dim.z);

                    if (Mathf.Clamp(placeToDraw.x, camPos.x, camDim.x) == placeToDraw.x) {
                        if (Mathf.Clamp(placeToDraw.y, camPos.y, camDim.y) == placeToDraw.y) {
                            Handles.BeginGUI();
                            Handles.Label(placeToDraw, props.box[i][j][(int) roomprops.dim.z].ToString(), style);
                            Handles.EndGUI();
                        }
                    }

                }
            }

        } catch (IndexOutOfRangeException) { }
        HandleUtility.Repaint();
    }

    override public void OnMenu() {

        

        /*
        if (GUI.changed) {
            props = MEGridProps.Instance;
        }

    */
        EditorGUILayout.Space();//Espacio Vertical
        GUILayout.Box(System.String.Empty, GUILayout.ExpandWidth(true), GUILayout.Height(4)); //Barra Horizontal
        EditorGUILayout.Space();//Espacio Vertical

        GUILayout.BeginHorizontal();
        GUILayout.Space(10); //Un simple espacio de margen horizontal
        EditorGUILayout.LabelField("Value: ", GUILayout.Width(40));
        props.selectedValue = EditorGUILayout.IntSlider(props.selectedValue, -10, 10, GUILayout.ExpandWidth(true));
        /*
        if (GUILayout.Button("Draw", GUILayout.ExpandWidth(true))) {
            drawGiz = !drawGiz;
        }
        */
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(10); //Un simple espacio de margen horizontal
        props.selectedValue = EditorGUILayout.IntSlider(props.selectedValue, 0, Mathf.RoundToInt(roomprops.dim.z / gridprops.size.x), GUILayout.Width(200));
        //props.layer = props.value * gridprops.size.x;
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(10); //Un simple espacio de margen horizontal
        if (GUILayout.Button("Save")) {
            byte[] byteArr = new byte[(props.box.GetLength(0) * props.box.GetLength(1)) * props.box.GetLength(2)];

            for (int i = 0; i < props.box.GetLength(0); i++) {
                for (int j = 0; j < props.box.GetLength(1); j++) {
                    for (int k = 0; k < props.box.GetLength(2); k++) {
                        int index = ((i * props.box.GetLength(1)) * props.box.GetLength(2)) + (j * props.box.GetLength(2)) + k;
                        byteArr[index] = Convert.ToByte(props.box[i][j][k]);
                    }
                }
            }
            //ms.Write(byteArr, 0, byteArr.Length); //EN MEMORIA

            string path = "Assets/Levels/" + SceneManager.GetActiveScene().name + "/CollisionMap" + ".rtf";
            FileStream stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
            BinaryWriter writer = new BinaryWriter(stream);
            for (int j = 0; j < byteArr.Length; j++) {
                writer.Write(byteArr[j]);
            }
            writer.Close();
            stream.Close();
            AssetDatabase.Refresh();

        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(10); //Un simple espacio de margen horizontal
        if (GUILayout.Button("Load Last Backup")) {
           // try { DumpBytesToArray3D(File.ReadAllBytes(Application.dataPath + "/Levels/" + SceneManager.GetActiveScene().name + "/CollisionMap" + ".rtf"), collisionArray); } catch (FileNotFoundException) { }
        }
        GUILayout.EndHorizontal();
        
    }

}
