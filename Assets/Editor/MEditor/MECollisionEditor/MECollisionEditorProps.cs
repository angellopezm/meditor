﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MECollisionEditorProps {

    private static MECollisionEditorProps instance = new MECollisionEditorProps();

    public static MECollisionEditorProps Instance { get { return instance; } set { instance = value; } }

    private MECollisionEditorProps() {
    }

    public int selectedValue;
    public int[][][] box;

}
