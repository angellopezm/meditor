﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

[MenuTab(Section = "General")]
public class MESceneManager : AbstractMEditorModule
{
    private static readonly Lazy<MESceneManager> lazy = new Lazy<MESceneManager>(() => CreateInstance<MESceneManager>());

    public static MESceneManager Instance { get { return lazy.Value; } }

    private void OnEnable()
    {
        SceneView.duringSceneGui -= OnSceneGUI;
        SceneView.duringSceneGui += OnSceneGUI;
    }

    public override void OnSceneGUI(SceneView sView)
    {
        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

        Tools.current = Tool.None;
    }

    public override void OnMenu()
    {
        GUIHelper.Row(() => {
            if (GUIHelper.CreateButton("Create Scene", GUILayout.ExpandWidth(true), GUILayout.Height(50)))
            {
                string sceneName = "";
                Vector2Int cellSize = new Vector2Int();
                Vector3 sceneSize = new Vector3();

                //CREATE WINDOW
                CreateInstance<PopupWindow>().Init("Create Scene", new Vector2(380, 250), (x) =>
                {
                    GUIHelper.Column(() => {
                        GUIHelper.Column(() => {
                            GUILayout.Label("Room name", EditorStyles.helpBox);
                            GUIHelper.Row(() => {
                                sceneName = (string)GUIHelper.CreateInput("Scene name:", sceneName, 200);
                            });
                        }, new StyleHelper().BoxBackground().style);

                        GUIHelper.Column(() => {
                            GUILayout.Label("Cell size", EditorStyles.helpBox);
                            GUIHelper.Row(() => {
                                cellSize.x = (int)GUIHelper.CreateInput("X:", cellSize.x, 50);
                                cellSize.y = (int)GUIHelper.CreateInput("Y:", cellSize.y, 50);
                            });
                        }, new StyleHelper().BoxBackground().style);

                        GUIHelper.Column(() => {
                            GUILayout.Label("Room size", EditorStyles.helpBox);
                            GUIHelper.Row(() => {
                                sceneSize.x = (float)GUIHelper.CreateInput("Width:", sceneSize.x, 50);
                                sceneSize.y = (float)GUIHelper.CreateInput("Height:", sceneSize.y, 50);
                                sceneSize.z = (float)GUIHelper.CreateInput("Depth:", sceneSize.z, 50);
                            });
                        }, new StyleHelper().BoxBackground().style);

                        GUILayout.Space(10);

                    }, new StyleHelper().BoxBackground().style, GUILayout.ExpandHeight(true));

                    GUIHelper.Row(() => {
                        if (GUIHelper.CreateButton("Accept"))
                        {
                            CreateInstance<PopupWindow>().Init("Confirmation", new Vector2(300, 200), (y) =>
                            {
                                GUIHelper.Column(() => {
                                    GUILayout.Label("Are you sure you want to create a scene with these settings?", EditorStyles.wordWrappedLabel);
                                }, new StyleHelper().BoxBackground().style, GUILayout.ExpandHeight(true));

                                GUIHelper.Row(() => {
                                    if (GUIHelper.CreateButton("Accept"))
                                    {
                                        Scene newScene = EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects, NewSceneMode.Single);
                                        newScene.name = sceneName;

                                        //EditorSceneManager.OpenScene(sceneName);
                                        y.Close();
                                        Debug.Log("Opened new Scene");
                                        GUIUtility.ExitGUI();
                                    }
                                    if (GUIHelper.CreateButton("Cancel"))
                                    {
                                        y.Close();
                                    }
                                });
                            });
                            
                        }
                        if (GUIHelper.CreateButton("Cancel"))
                        {
                            x.Close();
                        }
                    }, EditorStyles.helpBox);

                });

                /*
                Debug.Log("Created scene folder");
                Debug.Log("Saved scene");
                Debug.Log("Created tile mask");
                Debug.Log("Created collision mask");
                */
            }

        }, new StyleHelper().BoxBackground().style);
        

    }
}

