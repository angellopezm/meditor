﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System;

[MenuTab(Section = "Tiling")]
public class METilesetLoader: AbstractMEditorModule {

    private static readonly Lazy<METilesetLoader> lazy = new Lazy<METilesetLoader>(() => CreateInstance<METilesetLoader>());
    public METilesetLoaderProps props = METilesetLoaderProps.Instance;
    public MEGridProps gridprops = MEGridProps.Instance;

    public static METilesetLoader Instance { get { return lazy.Value; } }

    private METilesetLoader() {
    }

    // Put here your SceneView logic
    override public void OnSceneGUI(SceneView sView) {

    }

    // Don't erase this or this module won't work
    private void OnEnable() {
        /*
        SceneView.onSceneGUIDelegate -= OnSceneGUI;
        SceneView.onSceneGUIDelegate += OnSceneGUI;
        */
    }

    // Write here the Menu UI code
    override public void OnMenu() {

        props.toPath = "Assets/Resources/Tilesets/";

        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10);

        if (!Directory.Exists(props.toPath)) {
            AssetDatabase.CreateFolder("Assets/Resources", "Tilesets");
            AssetDatabase.Refresh();
            Debug.Log("Carpeta Tilesets creada. Ahi es donde puedes colocar tus tilesets");
            EditorUtility.DisplayDialog("Información", "Carpeta Tilesets creada. Puedes soltar tus tilesets en dicha carpeta o pulsando el botón 'Import'", "Ok");
        }

        props.files = Directory.GetFiles(props.toPath, "*.png");
        props.options = new string[props.files.Length];

        for (int i = 0; i < props.files.Length; i++) {
            props.options[i] = props.files[i].Replace(props.toPath, System.String.Empty);
        }

        props.selected = EditorGUILayout.Popup(props.selected, props.options, GUILayout.Width(250));

        if (GUILayout.Button(Resources.Load<Texture2D>("Icons/import"), GUILayout.Width(25), GUILayout.Height(25))) {
            try {
                props.path = EditorUtility.OpenFilePanel("Importa tus tilesets", "user/Pictures", "png");
                props.filename = Path.GetFileName(props.path);

                try {
                    File.Copy(props.path, props.toPath + props.filename);
                    AssetDatabase.Refresh();

                    Texture2D texture = Resources.Load<Texture2D>("Tilesets/" + props.filename.Substring(0, props.filename.Length - 4));

                    TexturePostProcessor tpp = new TexturePostProcessor();

                    tpp.OnPostprocessTexture(texture);

                    AssetDatabase.ImportAsset(props.toPath, ImportAssetOptions.ImportRecursive);

                    Debug.Log("Archivo importado correctamente.");
                } catch (System.ArgumentException) {
                    Debug.LogError("Ruta o nombre de archivo no seleccionado.");
                }

            } catch (IOException) {
                EditorUtility.DisplayDialog("Oops", "El archivo ya existe, por lo que no se volvera a importar.", "Ok");
            }
            AssetDatabase.Refresh();
        } //--------------------------------------------------------

        EditorGUILayout.EndHorizontal();
    }

}
