﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class METilesetLoaderProps {

    private static Lazy<METilesetLoaderProps> lazy = new Lazy<METilesetLoaderProps>(() => new METilesetLoaderProps());

    public static METilesetLoaderProps Instance { get { return lazy.Value; } set { lazy = new Lazy<METilesetLoaderProps>(() => value); } }

    private METilesetLoaderProps() {
    }

    public string[] files;
    public string[] options;
    public int selected;
    public string path;
    public string toPath;
    public string filename;

}
