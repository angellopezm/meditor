﻿using UnityEditor;
using System;
using System.Reflection;

public abstract class AbstractMEditorModule: Editor {
    abstract public void OnSceneGUI(SceneView sView);
    abstract public void OnMenu();

    public static implicit operator AbstractMEditorModule(Type v) {
        throw new NotImplementedException();
    }

    public FieldInfo GetModuleField(string name) {
        return GetType().GetField(name);
    }
}