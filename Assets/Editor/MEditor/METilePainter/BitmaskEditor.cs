﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

using System;
using UnityEditor.SceneManagement;

public class BitmaskEditor : SceneView
{
    private static readonly Lazy<BitmaskEditor> lazy = new Lazy<BitmaskEditor>(() => CreateWindow<BitmaskEditor>());
    Camera cam;

    public static BitmaskEditor Instance
    {
        get { return lazy.Value ; }
    }

    [MenuItem("Tools/BitmapEditor")]
    private static void Init()
    {
        Instance.Show();
        
    }

    override public void OnEnable()
    {
        //customScene = EditorSceneManager.GetSceneByPath("Assets/Scenes/SampleScene.unity");
        lastActiveSceneView.camera.scene = EditorSceneManager.GetSceneByPath("Assets/Scenes/SampleScene.unity");
    }

    private void OnInspectorUpdate()
    {
        Repaint();
    }

    public void OnSceneGUI(SceneView sView)
    {
       

    }

    override protected void OnGUI()
    {
        if (mouseOverWindow == Instance)
        {
            GUILayout.BeginHorizontal();
            Handles.DrawCamera(new Rect(0, 20, 100, 100), lastActiveSceneView.camera);
            GUILayout.EndHorizontal();
        }

    }

}
