﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class METilePainterProps {

    private static Lazy<METilePainterProps> lazy = new Lazy<METilePainterProps>(() => new METilePainterProps());
    public static METilePainterProps Instance { get { return lazy.Value; } set { lazy = new Lazy<METilePainterProps>(() => value); } }

    private METilePainterProps() {
    }

    public enum EDITOOLS {
        move = 0,
        paint = 1,
        paintover = 2,
        erase = 3,
        pipette = 4,
        bucket = 5
    }

    public int[][][] mask;
    public string toolMessage = "No tool selected";
    public EDITOOLS tool, lastTool;
    public float layer;
    public bool expanded = true, allowEdit = false, renderCursor = true, focusLayer = false, flipX = false, flipY = false;
    public Sprite[] allSprites;
    public Sprite activeSprite;
    public Vector2 mousePos;
    public Vector3 mouseWorldPos;
    public GameObject activeGo;
    public List<GameObject> allTileGo;
    public Ray ray;
    public RaycastHit hit;
    public int value = 0;
    public Texture2D tileset;


    public GUIStyle textureStyle;
    public GUIStyle textureStyleAct;
}
