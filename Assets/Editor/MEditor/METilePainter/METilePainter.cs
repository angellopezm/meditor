﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

[MenuTab(Section = "Tiling")]
public class METilePainter : AbstractMEditorModule
{

    private static readonly Lazy<METilePainter> lazy = new Lazy<METilePainter>(() => CreateInstance<METilePainter>());
    public MEGridProps gridprops = MEGridProps.Instance;
    public METilePainterProps props = METilePainterProps.Instance;
    public METilesetLoaderProps tilesetloaderprops = METilesetLoaderProps.Instance;
    public MERoomProps roomprops = MERoomProps.Instance;
    public MEditorTilesetWindow tilesetWindow;
    Texture2D[] cursorTexture = new Texture2D[6];

    public static METilePainter Instance { get { return lazy.Value; } }

    private METilePainter()
    {
    }

    // Don't erase this or this module won't work
    private void OnEnable()
    {
        //PRELOAD CURSOR TEXTURES
        cursorTexture[0] = Resources.Load<Texture2D>("Icons/move");
        cursorTexture[1] = Resources.Load<Texture2D>("Icons/paint");
        cursorTexture[2] = Resources.Load<Texture2D>("Icons/corrector");
        cursorTexture[3] = Resources.Load<Texture2D>("Icons/eraser");
        cursorTexture[4] = Resources.Load<Texture2D>("Icons/pipette");
        cursorTexture[5] = Resources.Load<Texture2D>("Icons/bucket");

        SceneView.duringSceneGui -= OnSceneGUI;
        SceneView.duringSceneGui += OnSceneGUI;
    }

    // Put here your SceneView logic
    override public void OnSceneGUI(SceneView sView)
    {
        Repaint();

        tilesetWindow = MEditorTilesetWindow.Instance;

        FocusLayer();

        if (props.allowEdit)
        {

            Render();

            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

            Tools.current = Tool.None;
            Event e = Event.current;

            props.lastTool = props.tool;

            if (e.button == 1)
            {
                props.allowEdit = true;
                props.tool = METilePainterProps.EDITOOLS.pipette;
            }

            props.mousePos = Event.current.mousePosition;
            props.mousePos.y = SceneView.currentDrawingSceneView.camera.pixelHeight - props.mousePos.y;
            props.mouseWorldPos = SceneView.currentDrawingSceneView.camera.ScreenPointToRay(props.mousePos).origin;

            if (gridprops.size.x >= .16f && gridprops.size.y >= .16f)
            {
                props.mouseWorldPos.x = (Mathf.Floor(props.mouseWorldPos.x / gridprops.size.x) * gridprops.size.x);
                props.mouseWorldPos.y = (Mathf.Ceil(props.mouseWorldPos.y / gridprops.size.y) * gridprops.size.y);
            }
            props.mouseWorldPos.z = props.layer;

            Selection.activeGameObject = null;

            Camera cam = SceneView.currentDrawingSceneView.camera;

            cam.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y, (props.mouseWorldPos.z - gridprops.size.x) + .01f);

            props.ray = SceneView.currentDrawingSceneView.camera.ScreenPointToRay(new Vector3(props.mousePos.x, props.mousePos.y, props.mouseWorldPos.z));
            //Debug.DrawRay(props.ray.origin, props.ray.direction, Color.green);

            switch (props.tool)
            {
                case METilePainterProps.EDITOOLS.move:

                    Cursor.SetCursor(cursorTexture[0], Vector2.zero, CursorMode.Auto);

                    if (MERoom.CheckIfInside(roomprops, gridprops, props.mouseWorldPos))
                    {
                        if (Physics.Raycast(props.ray, out props.hit, Vector3.Distance(props.mouseWorldPos, props.ray.origin)))
                        {
                            if ((e.type == EventType.MouseDown) && e.button == 0)
                            {
                                if (props.hit.collider.gameObject.transform.position.z == props.mouseWorldPos.z)
                                {
                                    props.activeGo = props.hit.collider.gameObject;
                                }
                            }
                        }
                    }
                    if ((e.type == EventType.MouseDrag || e.type == EventType.MouseDown) && e.button == 0)
                    {
                        try
                        {
                            props.activeGo.transform.position = props.mouseWorldPos;
                        }
                        catch (NullReferenceException) { }
                    }

                    break;

                case METilePainterProps.EDITOOLS.paint:

                    Cursor.SetCursor(cursorTexture[1], Vector2.zero, CursorMode.Auto);

                    if (MERoom.CheckIfInside(roomprops, gridprops, props.mouseWorldPos))
                    {
                        if (!Physics.Raycast(props.ray, out props.hit, Vector3.Distance(props.mouseWorldPos, props.ray.origin)))
                        {
                            if ((e.type == EventType.MouseDrag || e.type == EventType.MouseDown) && e.button == 0 && props.activeSprite != null)
                            {

                                CreateTile(null, props.activeSprite, props.activeSprite.name, props.mouseWorldPos);

                                /* OBSOLETE
                                if (props.flipX)
                                {
                                    newgo.GetComponent<SpriteRenderer>().transform.position = new Vector3(props.mouseWorldPos.x + gridprops.size.x / 100, props.mouseWorldPos.y, props.mouseWorldPos.z);
                                    newgo.GetComponent<SpriteRenderer>().flipX = props.flipX;
                                    if (props.flipY)
                                    {
                                        newgo.GetComponent<SpriteRenderer>().transform.position = new Vector3(props.mouseWorldPos.x + gridprops.size.x / 100, props.mouseWorldPos.y - gridprops.size.y / 100, props.mouseWorldPos.z);
                                        newgo.GetComponent<SpriteRenderer>().flipY = props.flipY;
                                    }
                                }
                                else if (props.flipY)
                                {
                                    newgo.GetComponent<SpriteRenderer>().transform.position = new Vector3(props.mouseWorldPos.x, props.mouseWorldPos.y - gridprops.size.y / 100, props.mouseWorldPos.z);
                                    newgo.GetComponent<SpriteRenderer>().flipY = props.flipY;
                                    if (props.flipX)
                                    {
                                        newgo.GetComponent<SpriteRenderer>().transform.position = new Vector3(props.mouseWorldPos.x + gridprops.size.x / 100, props.mouseWorldPos.y - gridprops.size.y / 100, props.mouseWorldPos.z);
                                        newgo.GetComponent<SpriteRenderer>().flipX = props.flipX;
                                    }
                                }
                                */

                            }
                        }
                    }
                    break;

                case METilePainterProps.EDITOOLS.paintover:

                    Cursor.SetCursor(cursorTexture[2], Vector2.zero, CursorMode.Auto);

                    if (Physics.Raycast(props.ray, out props.hit, Vector3.Distance(props.mouseWorldPos, props.ray.origin)))
                    {
                        if ((e.type == EventType.MouseDrag || e.type == EventType.MouseDown) && e.button == 0 && props.activeSprite != null)
                        {
                            DestroyImmediate(props.hit.collider.gameObject);
                        }
                    }

                    if (MERoom.CheckIfInside(roomprops, gridprops, props.mouseWorldPos))
                    {
                        if (!Physics.Raycast(props.ray, out props.hit, Vector3.Distance(props.mouseWorldPos, props.ray.origin)))
                        {
                            if ((e.type == EventType.MouseDrag || e.type == EventType.MouseDown) && e.button == 0 && props.activeSprite != null)
                            {

                                CreateTile(null, props.activeSprite, props.activeSprite.name, props.mouseWorldPos);

                                /* OBSOLETE
                                if (props.flipX)
                                {
                                    newgo.GetComponent<SpriteRenderer>().transform.position = new Vector3(props.mouseWorldPos.x + (gridprops.size.x / 100), props.mouseWorldPos.y, props.mouseWorldPos.z);
                                    newgo.GetComponent<SpriteRenderer>().flipX = props.flipX;
                                    if (props.flipY)
                                    {
                                        newgo.GetComponent<SpriteRenderer>().transform.position = new Vector3(props.mouseWorldPos.x + (gridprops.size.x / 100), props.mouseWorldPos.y - (gridprops.size.y / 100), props.mouseWorldPos.z);
                                        newgo.GetComponent<SpriteRenderer>().flipY = props.flipY;
                                    }
                                }
                                else if (props.flipY)
                                {
                                    newgo.GetComponent<SpriteRenderer>().transform.position = new Vector3(props.mouseWorldPos.x, props.mouseWorldPos.y - (gridprops.size.y / 100), props.mouseWorldPos.z);
                                    newgo.GetComponent<SpriteRenderer>().flipY = props.flipY;
                                    if (props.flipX)
                                    {
                                        newgo.GetComponent<SpriteRenderer>().transform.position = new Vector3(props.mouseWorldPos.x + (gridprops.size.x / 100), props.mouseWorldPos.y - (gridprops.size.y / 100), props.mouseWorldPos.z);
                                        newgo.GetComponent<SpriteRenderer>().flipX = props.flipX;
                                    }
                                }
                                */

                            }
                        }
                    }

                    break;

                case METilePainterProps.EDITOOLS.erase:

                    Cursor.SetCursor(cursorTexture[3], Vector2.zero, CursorMode.Auto);

                    if (Physics.Raycast(props.ray, out props.hit, Vector3.Distance(props.mouseWorldPos, props.ray.origin)))
                    {
                        if ((e.type == EventType.MouseDrag || e.type == EventType.MouseDown) && e.button == 0)
                        {
                            if (props.hit.collider.gameObject.transform.position.z == props.mouseWorldPos.z)
                            {
                                DestroyImmediate(props.hit.collider.gameObject);
                            }
                        }
                    }
                    break;

                case METilePainterProps.EDITOOLS.pipette:

                    Cursor.SetCursor(cursorTexture[4], Vector2.zero, CursorMode.Auto);

                    if (props.lastTool != METilePainterProps.EDITOOLS.pipette)
                    {
                        if (Physics.Raycast(props.ray, out props.hit, gridprops.size.x))
                        {
                            if (e.type == EventType.MouseDown && (e.button == 0 || e.button == 1))
                            {
                                if (props.hit.collider.gameObject.transform.position.z == props.mouseWorldPos.z)
                                {
                                    props.activeSprite = props.hit.collider.gameObject.GetComponent<SpriteRenderer>().sprite;

                                }

                            }

                        }
                        props.tool = props.lastTool;
                    }
                    else
                    {
                        if (Physics.Raycast(props.ray, out props.hit, gridprops.size.x))
                        {
                            if (e.type == EventType.MouseDown && (e.button == 0 || e.button == 1))
                            {
                                if (props.hit.collider.gameObject.transform.position.z == props.mouseWorldPos.z)
                                {
                                    props.activeSprite = props.hit.collider.gameObject.GetComponent<SpriteRenderer>().sprite;

                                }

                            }

                        }
                    }

                    break;

                case METilePainterProps.EDITOOLS.bucket:


                    Cursor.SetCursor(cursorTexture[5], Vector2.zero, CursorMode.Auto);

                    Sprite underneathSprite = null;
                    List<Vector3> toChange = new List<Vector3>();
                    Ray ray = new Ray();
                    RaycastHit hit;
                    int maxSize = 0;
                    Vector3 pos;
                    int cap = 0; //Limit is 2 times

                    if (props.activeSprite != null)
                    {
                        // 3 layers of raycasts| Variables used per layer (x => y => z)
                        if (Physics.Raycast(props.ray, out props.hit, gridprops.size.x))
                        {
                            underneathSprite = props.hit.collider.gameObject.GetComponent<SpriteRenderer>().sprite;
                            pos = props.hit.collider.gameObject.transform.position;
                            if (e.type == EventType.MouseDown && (e.button == 0 || e.button == 1))
                            {
                                toChange.Add(pos);

                                while ((maxSize < toChange.Count) && cap != 2)
                                {

                                    foreach (Vector3 position in toChange.ToList())
                                    {
                                        ray = new Ray(new Vector3((position.x + .01f), (position.y - .01f), props.layer - .01f), Vector3.forward);

                                        // Center
                                        PerformRaycast((y) =>
                                        {

                                            // Upper
                                            pos = new Vector3((position.x + .01f), (position.y - .01f) + gridprops.size.y, props.layer - .01f);
                                            ray = new Ray(pos, Vector3.forward);
                                            Debug.DrawRay(ray.origin, ray.direction, Color.white, 10);

                                            PerformRaycast((z) =>
                                            {
                                                if (z.collider.gameObject.GetComponent<SpriteRenderer>().sprite != underneathSprite != props.activeSprite)
                                                {
                                                    if (!toChange.Contains(z.collider.transform.position))
                                                    {
                                                        toChange.Add(z.collider.transform.position);
                                                    }
                                                }
                                            }, ray, y, gridprops.size.x, true);

                                            // Right
                                            pos = new Vector3((position.x + .01f) + gridprops.size.x, (position.y - .01f), props.layer - .01f);
                                            ray = new Ray(pos, Vector3.forward);
                                            Debug.DrawRay(ray.origin, ray.direction, Color.white, 10);

                                            PerformRaycast((z) =>
                                            {
                                                if (z.collider.gameObject.GetComponent<SpriteRenderer>().sprite != underneathSprite != props.activeSprite)
                                                {
                                                    if (!toChange.Contains(z.collider.transform.position))
                                                    {
                                                        toChange.Add(z.collider.transform.position);
                                                    }
                                                }
                                            }, ray, y, gridprops.size.x, true);

                                            // Bottom
                                            pos = new Vector3((position.x + .01f), (position.y - .01f) - gridprops.size.y, props.layer - .01f);
                                            ray = new Ray(pos, Vector3.forward);
                                            Debug.DrawRay(ray.origin, ray.direction, Color.white, 10);

                                            PerformRaycast((z) =>
                                            {
                                                if (z.collider.gameObject.GetComponent<SpriteRenderer>().sprite != underneathSprite != props.activeSprite)
                                                {
                                                    if (!toChange.Contains(z.collider.transform.position))
                                                    {
                                                        toChange.Add(z.collider.transform.position);
                                                    }
                                                }
                                            }, ray, y, gridprops.size.x, true);

                                            // Left
                                            pos = new Vector3((position.x + .01f) - gridprops.size.x, (position.y - .01f), props.layer - .01f);
                                            ray = new Ray(pos, Vector3.forward);
                                            Debug.DrawRay(ray.origin, ray.direction, Color.white, 10);

                                            PerformRaycast((z) =>
                                            {
                                                if (z.collider.gameObject.GetComponent<SpriteRenderer>().sprite != underneathSprite != props.activeSprite)
                                                {
                                                    if (!toChange.Contains(z.collider.transform.position))
                                                    {
                                                        toChange.Add(z.collider.transform.position);
                                                    }
                                                }
                                            }, ray, y, gridprops.size.x, true);
                                        }, ray, props.hit, gridprops.size.x, true);

                                    }

                                    if (maxSize == toChange.Count())
                                    {
                                        cap++;
                                    }

                                    maxSize++;
                                }
                            }
                            foreach (Vector3 position in toChange)
                            {
                                ray = new Ray(new Vector3((position.x + .01f), (position.y - .01f), props.layer - .01f), Vector3.forward);
                                hit = new RaycastHit();

                                PerformRaycast((x) =>
                                {
                                    CreateTile(x.collider.gameObject, props.activeSprite, props.activeSprite.name, position);
                                }, ray, hit, gridprops.size.x, true);
                            }
                            toChange = new List<Vector3>();
                        }

                        // IF THERES NOTHING BELOW
                        if (!Physics.Raycast(props.ray, out props.hit, gridprops.size.x))
                        {

                            if (e.type == EventType.MouseDown && (e.button == 0 || e.button == 1))
                            {
                                pos = new Vector3(Adjust(props.mouseWorldPos.x, gridprops.size.x), Adjust(props.mouseWorldPos.y, gridprops.size.y), props.layer);
                                hit = new RaycastHit();
                                toChange.Add(pos);

                                while ((maxSize < toChange.Distinct().Count()) && cap != 2)
                                {

                                    foreach (Vector3 position in toChange.ToList())
                                    {
                                        if (MERoom.CheckIfInside(roomprops, gridprops, position))
                                        {
                                            pos = new Vector3(Adjust(position.x, gridprops.size.x), Adjust(position.y, gridprops.size.y), props.layer);
                                            ray = new Ray(pos, Vector3.forward);

                                            // Center
                                            PerformRaycast((y) =>
                                            {

                                                // Upper
                                                //pos = new Vector3(position.x + gridprops.size.x, position.y, props.layer);
                                                pos = new Vector3((position.x + .01f), (position.y + gridprops.size.y) - .01f, props.layer - .01f);
                                                ray = new Ray(pos, Vector3.forward);

                                                PerformRaycast((z) =>
                                                {
                                                    pos = new Vector3(position.x, position.y + gridprops.size.y, props.layer);
                                                    if (!toChange.Contains(pos) && MERoom.CheckIfInside(roomprops, gridprops, pos))
                                                    {
                                                        toChange.Add(pos);
                                                    }
                                                }, ray, y, gridprops.size.x, false);

                                                // Right
                                                //pos = new Vector3(position.x + gridprops.size.x, position.y, props.layer);
                                                pos = new Vector3((position.x + gridprops.size.x) + .01f, (position.y - .01f), props.layer - .01f);
                                                ray = new Ray(pos, Vector3.forward);

                                                PerformRaycast((z) =>
                                                {
                                                    pos = new Vector3(position.x + gridprops.size.x, position.y, props.layer);
                                                    if (!toChange.Contains(pos) && MERoom.CheckIfInside(roomprops, gridprops, pos))
                                                    {
                                                        toChange.Add(pos);
                                                    }
                                                }, ray, y, gridprops.size.x, false);

                                                // Bottom
                                                //pos = new Vector3(position.x + gridprops.size.x, position.y, props.layer);
                                                pos = new Vector3((position.x + .01f), (position.y - gridprops.size.y) - .01f, props.layer - .01f);
                                                ray = new Ray(pos, Vector3.forward);
                                                Debug.DrawRay(ray.origin, ray.direction, Color.white, 10);

                                                PerformRaycast((z) =>
                                                {
                                                    pos = new Vector3(position.x, position.y - gridprops.size.y, props.layer);
                                                    if (!toChange.Contains(pos) && MERoom.CheckIfInside(roomprops, gridprops, pos))
                                                    {
                                                        toChange.Add(pos);
                                                    }
                                                }, ray, y, gridprops.size.x, false);

                                                // Left
                                                //pos = new Vector3(position.x + gridprops.size.x, position.y, props.layer);
                                                pos = new Vector3((position.x - gridprops.size.x) + .01f, (position.y - .01f), props.layer - .01f);
                                                ray = new Ray(pos, Vector3.forward);

                                                PerformRaycast((z) =>
                                                {
                                                    pos = new Vector3(position.x - gridprops.size.x, position.y, props.layer);
                                                    if (!toChange.Contains(pos) && MERoom.CheckIfInside(roomprops, gridprops, pos))
                                                    {
                                                        toChange.Add(pos);
                                                    }
                                                }, ray, y, gridprops.size.x, false);
                                            }, ray, hit, gridprops.size.x, false);
                                        }
                                        else {
                                            toChange.Remove(position);
                                        }
                                    }

                                    if (maxSize == toChange.Count())
                                    {
                                        cap++;
                                    }

                                    maxSize++;
                                }
                            }

                            foreach (Vector3 position in toChange.Distinct().ToList())
                            {
                                var aux = new Vector3(position.x + .01f, position.y - .01f, props.layer - .01f);
                                ray = new Ray(aux, Vector3.forward);
                                PerformRaycast((x) =>
                                {
                                    if (MERoom.CheckIfInside(roomprops, gridprops, aux))
                                    {
                                        Debug.Log(toChange.Count);
                                        CreateTile(null, props.activeSprite, props.activeSprite.name, position);
                                    }

                                }, ray, new RaycastHit(), gridprops.size.x, false);

                            }
                        }
                        toChange = new List<Vector3>();
                    }
                    break;

                default:

                    try
                    {
                        if (props.activeGo.transform.position.z != props.mouseWorldPos.z)
                        {
                            props.activeGo = null;
                        }
                    }
                    catch (NullReferenceException) { }
                    break;
            }
            EditorGUIUtility.AddCursorRect(SceneView.lastActiveSceneView.camera.pixelRect, MouseCursor.CustomCursor);
        }
        else
        {

            EditorGUIUtility.AddCursorRect(SceneView.lastActiveSceneView.camera.pixelRect, MouseCursor.Arrow);
        }

    }

    private void Render()
    {
        CursorBehavior();
    }

    private void FocusLayer()
    {
        props.allTileGo = GameObject.FindGameObjectsWithTag("Tile").ToList();
        if (props.focusLayer)
        {
            for (int i = 0; i < props.allTileGo.Count; i++)
            {
                if (props.allTileGo[i].transform.position.z != props.layer)
                {
                    if (props.allTileGo[i].GetComponent<SpriteRenderer>().enabled)
                    {
                        props.allTileGo[i].GetComponent<SpriteRenderer>().enabled = false;
                    }
                    else
                    {
                        props.allTileGo[i].GetComponent<SpriteRenderer>().enabled = true;
                    }
                }
            }
        }
    }

    private void CursorBehavior()
    {
        if (Physics.Raycast(props.ray, out props.hit, gridprops.size.x))
        {
            float H, S, V;
            Sprite hitSprite = props.hit.collider.gameObject.GetComponent<SpriteRenderer>().sprite;
            Color32 textureColor = hitSprite.texture.GetPixel((int)hitSprite.rect.x, (int)hitSprite.textureRect.y + (int)hitSprite.textureRect.height);
            textureColor.a = 255;
            Color.RGBToHSV(textureColor, out H, out S, out V);
            H = (H + .5f) % 1;
            Color32 col = Color.HSVToRGB(H, Mathf.Ceil(S), V);
            Handles.color = col;

        }
        else
        {
            Handles.color = Color.white;
        }

        if (props.renderCursor)
        {
            Handles.DrawWireCube(new Vector3(props.mouseWorldPos.x + gridprops.size.x / 2, props.mouseWorldPos.y - gridprops.size.y / 2, 0), new Vector3(gridprops.size.x, gridprops.size.y, 0));
        }
    }

    // Write here the Menu UI code
    override public void OnMenu()
    {

        if (GUI.changed)
        {
            gridprops = MEGridProps.Instance;
            roomprops = MERoomProps.Instance;
            props = METilePainterProps.Instance;
        }

        props.textureStyleAct = new GUIStyle(GUI.skin.button);
        props.textureStyleAct.margin = new RectOffset(0, 0, 0, 0);
        props.textureStyleAct.normal.background = null;
        props.textureStyleAct.hover.background = Resources.Load<Texture2D>("Icons/selected");
        props.textureStyleAct.active.background = Resources.Load<Texture2D>("Icons/selected");
        props.textureStyleAct.focused.background = Resources.Load<Texture2D>("Icons/selected");

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10); //Un simple espacio de margen horizontal
        EditorGUILayout.LabelField("Enable paint ", GUILayout.Width(76)); //El texto "Enable paint". Sencillamente
        props.allowEdit = EditorGUILayout.Toggle(props.allowEdit);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10); //Un simple espacio de margen horizontal
        EditorGUILayout.LabelField("Enable cursor ", GUILayout.Width(82));
        props.renderCursor = EditorGUILayout.Toggle(props.renderCursor);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10); //Un simple espacio de margen horizontal
        if (GUILayout.Button(cursorTexture[1], GUILayout.Width(25), GUILayout.Height(25)))
        {
            props.allowEdit = true;
            props.tool = METilePainterProps.EDITOOLS.paint;
        }
        else if (GUILayout.Button(cursorTexture[2], GUILayout.Width(25), GUILayout.Height(25)))
        {
            props.allowEdit = true;
            props.tool = METilePainterProps.EDITOOLS.paintover;
        }
        else if (GUILayout.Button(cursorTexture[3], GUILayout.Width(25), GUILayout.Height(25)))
        {
            props.allowEdit = true;
            props.tool = METilePainterProps.EDITOOLS.erase;
        }
        else if (GUILayout.Button(cursorTexture[0], GUILayout.Width(25), GUILayout.Height(25)))
        {
            props.allowEdit = true;
            props.tool = METilePainterProps.EDITOOLS.move;
        }
        else if (GUILayout.Button(cursorTexture[4], GUILayout.Width(25), GUILayout.Height(25)))
        {
            props.allowEdit = true;
            props.tool = METilePainterProps.EDITOOLS.pipette;
        }
        else if (GUILayout.Button(cursorTexture[5], GUILayout.Width(25), GUILayout.Height(25)))
        {
            props.allowEdit = true;
            props.tool = METilePainterProps.EDITOOLS.bucket;
        }

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10); //Un simple espacio de margen horizontal
        props.tool = (METilePainterProps.EDITOOLS)EditorGUILayout.EnumPopup(props.tool, GUILayout.Width(236));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10);

        switch (props.tool)
        {
            case METilePainterProps.EDITOOLS.paint:
                props.toolMessage = "Brush";
                break;
            case METilePainterProps.EDITOOLS.paintover:
                props.toolMessage = "Paintover";
                break;
            case METilePainterProps.EDITOOLS.erase:
                props.toolMessage = "Erase";
                break;
            case METilePainterProps.EDITOOLS.move:
                props.toolMessage = "Move";
                break;
            case METilePainterProps.EDITOOLS.pipette:
                props.toolMessage = "Pipette";
                break;
            case METilePainterProps.EDITOOLS.bucket:
                props.toolMessage = "Bucket";
                break;
            default:
                props.toolMessage = "No tool selected";
                break;
        }

        EditorGUILayout.LabelField("Selected tool: " + props.toolMessage, GUILayout.Width(150));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10);
        EditorGUILayout.LabelField("Depth: ", GUILayout.Width(40));
        props.value = EditorGUILayout.IntSlider(props.value, 0, Mathf.RoundToInt(roomprops.dim.z / gridprops.size.x), GUILayout.Width(200));
        props.layer = props.value * gridprops.size.x;
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10);
        EditorGUILayout.LabelField("Real Layer: " + props.layer, GUILayout.Width(100));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10); //Un simple espacio de margen horizontal
        EditorGUILayout.LabelField("Focus layer", GUILayout.Width(68));
        props.focusLayer = EditorGUILayout.Toggle(props.focusLayer);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10);
        EditorGUILayout.LabelField("Selected tile → ", GUILayout.Width(90));
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.TextField("", GUILayout.Width(32), GUILayout.Height(32));
        EditorGUI.EndDisabledGroup();

        if (props.activeSprite != null)
        {

            GUI.DrawTextureWithTexCoords(new Rect(GUILayoutUtility.GetLastRect().x + 1,
                                                  GUILayoutUtility.GetLastRect().y + 1,
                                                  GUILayoutUtility.GetLastRect().width - 2,
                                                  GUILayoutUtility.GetLastRect().height - 2),
                                         props.activeSprite.texture, new Rect(
                                                            props.activeSprite.rect.x / props.activeSprite.texture.width,
                                                            props.activeSprite.rect.y / props.activeSprite.texture.height,
                                                            props.activeSprite.rect.width / props.activeSprite.texture.width,
                                                            props.activeSprite.rect.height / props.activeSprite.texture.height));
        }

        EditorGUILayout.EndHorizontal();

        /*
        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10);
        EditorGUILayout.LabelField("Flip (x/y): ", GUILayout.Width(60));
        props.flipX = EditorGUILayout.Toggle(props.flipX, GUILayout.Width(10), GUILayout.Height(20));
        props.flipY = EditorGUILayout.Toggle(props.flipY, GUILayout.Width(10), GUILayout.Height(20));
        EditorGUILayout.EndHorizontal();
        */
        GUILayout.Space(10);
        GUILayout.Space(10);
    }

    private void PerformRaycast(Action<RaycastHit> content, Ray ray, RaycastHit hit, float maxDistance, bool isTrue)
    {
        if (isTrue)
        {
            if (Physics.Raycast(ray, out hit, maxDistance))
            {
                content.Invoke(hit);
            }
        }
        else
        {
            if (!Physics.Raycast(ray, out hit, maxDistance))
            {
                content.Invoke(hit);
            }
        }
    }

    private void CreateTile(GameObject toDestroy, Sprite sprite, string spriteName, Vector3 position)
    {

        GameObject newgo = new GameObject(spriteName, typeof(SpriteRenderer));

        if (toDestroy != null)
        {
            DestroyImmediate(toDestroy);
        }
        newgo.transform.position = position;

        newgo.GetComponent<SpriteRenderer>().sprite = sprite;
        newgo.GetComponent<Renderer>().material = Resources.Load<Material>("Materials/Sprite");

        newgo.AddComponent<BoxCollider>();
        newgo.GetComponent<BoxCollider>().size = new Vector3(gridprops.size.x, gridprops.size.y, .01f);
        newgo.GetComponent<BoxCollider>().isTrigger = true;
        newgo.tag = "Tile";
    }

    private float Adjust(float number, float ratio)
    {
        return Mathf.Round(number * (1 / ratio)) / (1 / ratio);
    }
}