﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEditorInternal;

[CanEditMultipleObjects]
public class MEditorTilesetWindow : EditorWindow
{
    private static readonly Lazy<MEditorTilesetWindow> lazy = new Lazy<MEditorTilesetWindow>(() => GetWindow<MEditorTilesetWindow>());

    private Vector2 scrollPos;
    public MEGridProps gridprops = MEGridProps.Instance;
    public METilePainterProps props = METilePainterProps.Instance;
    public METilesetLoaderProps tilesetloaderprops = METilesetLoaderProps.Instance;

    public Rect windowRect;

    public MERoomProps roomprops = MERoomProps.Instance;

    int tab;


    public static MEditorTilesetWindow Instance { get { return lazy.Value; } }

    private MEditorTilesetWindow()
    {
    }

    [MenuItem("Tools/Tileset")]
    private static void Init()
    {

    }


    private void OnEnable()
    {
    }

    private void OnInspectorUpdate()
    {
        Instance.titleContent = new GUIContent("Tilesets & Misc");
        try
        {
            minSize = new Vector2(props.tileset.width + 14, props.tileset.width);
        }
        catch (NullReferenceException) { }
        Repaint();

    }

    private void OnGUI()
    {
        tab = GUILayout.Toolbar(tab, new string[] { "Tileset", "SmartTile" });

        switch (tab)
        {

            case 0:

                ShowTilesetWindow();
                break;

            case 1:
                ShowSmartTileWindow();
                break;

        }
    }

    private void ShowSmartTileWindow()
    {

        GUILayout.Space(10);

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10);
        GUILayout.Space(10);

        try
        {

            if (props.activeSprite != null) {
                if(GUILayout.Button(Resources.Load<Texture2D>("Icons/plus"), GUILayout.Width(35), GUILayout.Height(35)))
                {
                    
                }
                GUILayout.Label(new GUIContent("Add selected Tile to a SmartGroup"));
            }

        }
        catch (ArgumentNullException) { }

        EditorGUILayout.EndHorizontal();
    }

    private void ShowTilesetWindow()
    {
        Event e = Event.current;

        try
        {
            string tilesetFilename = tilesetloaderprops.options[tilesetloaderprops.selected];

            props.tileset = Resources.Load<Texture2D>("Tilesets/" + tilesetFilename.Substring(0, tilesetFilename.Length - 4));

            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            GUILayout.BeginHorizontal();

            float off = 0f;

            if (tilesetloaderprops.options.Length > tilesetloaderprops.selected)
            {
                props.allSprites = AssetDatabase.LoadAllAssetsAtPath("Assets/Resources/Tilesets/" + tilesetFilename).Select(x => x as Sprite).Where(x => x != null).OrderByDescending(x => x.rect.y).ToArray();

                foreach (Sprite singsprite in props.allSprites)
                {
                    if (off >= props.tileset.width)
                    {
                        off = 0f;
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal();
                    }
                    if (props.activeSprite == singsprite)
                    {
                        GUILayout.Button(System.String.Empty, props.textureStyleAct, GUILayout.Width(singsprite.rect.width), GUILayout.Height(singsprite.rect.height));
                        GUI.DrawTextureWithTexCoords(new Rect(GUILayoutUtility.GetLastRect().x + 1,
                                                              GUILayoutUtility.GetLastRect().y + 1,
                                                              GUILayoutUtility.GetLastRect().width - 2,
                                                              GUILayoutUtility.GetLastRect().height - 2),
                                                     singsprite.texture,
                                                     new Rect(
                                                            singsprite.rect.x / singsprite.texture.width,
                                                            singsprite.rect.y / singsprite.texture.height,
                                                            singsprite.rect.width / singsprite.texture.width,
                                                            singsprite.rect.height / singsprite.texture.height));

                    }
                    else
                    {
                        if (GUILayout.Button(System.String.Empty, props.textureStyleAct, GUILayout.Width(singsprite.rect.width), GUILayout.Height(singsprite.rect.height))) {
                            props.activeSprite = singsprite;
                        }
                        GUI.DrawTextureWithTexCoords(GUILayoutUtility.GetLastRect(), singsprite.texture,
                           new Rect(singsprite.rect.x / singsprite.texture.width,
                           singsprite.rect.y / singsprite.texture.height,
                           singsprite.rect.width / singsprite.texture.width,
                           singsprite.rect.height / singsprite.texture.height));
                    }
                    off += singsprite.rect.width;
                }
            }
            GUILayout.EndHorizontal();

            EditorGUILayout.EndScrollView();

        }
        catch (NullReferenceException)
        {
        }

    }
}
