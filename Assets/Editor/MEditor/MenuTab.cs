﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuTab : Attribute {

    private string section;

    public string Section {
        get {
            return section;
        }

        set {
            section = value;
        }
    }
}
