﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MERoomProps {

    private static MERoomProps instance = new MERoomProps();

    public static MERoomProps Instance { get { return instance; } set { instance = value; } }

    private MERoomProps() {
    }

    public Vector3 dim = new Vector3(0, 0, 0);
    public bool expanded = true, limit = true;
    public int floorInterval;
}
