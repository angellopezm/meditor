﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

[MenuTab(Section = "General")]
public class MERoom : AbstractMEditorModule
{

    private static readonly Lazy<MERoom> lazy = new Lazy<MERoom>(() => CreateInstance<MERoom>());
    public MEGridProps gridprops = MEGridProps.Instance;
    public MERoomProps props = MERoomProps.Instance;
    public string path;

    public static MERoom Instance { get { return lazy.Value; } }

    private MERoom()
    {
    }

    private void OnEnable()
    {

        path = Application.dataPath + "/Editor/MEditor/" + GetType().Name + "/conf.json";

        if (File.Exists(path))
        {
            MERoomProps.Instance = Utils.Load<MERoomProps>(path);
        }
        props = MERoomProps.Instance;

        SceneView.duringSceneGui -= OnSceneGUI;
        SceneView.duringSceneGui += OnSceneGUI;
    }

    override public void OnSceneGUI(SceneView sView)
    {
        SnapResize();
        Render();

        if (props.limit)
        {
            CheckBounds();
        }
    }

    private void Render()
    {
        Handles.color = Color.red;
        Handles.DrawWireCube(new Vector3(props.dim.x / 2, props.dim.y / 2, props.dim.z / 2), new Vector3(props.dim.x, props.dim.y, props.dim.z));
    }

    override public void OnMenu()
    {

        if (GUI.changed)
        {
            gridprops = MEGridProps.Instance;
            props = MERoomProps.Instance;
        }

        EditorGUILayout.Space();
        props.expanded = EditorGUILayout.Foldout(props.expanded, "Room");

        if (props.expanded)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(30);
            props.dim = EditorGUILayout.Vector3Field("Room Size: ", props.dim, GUILayout.Width(200));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(30);
            EditorGUILayout.LabelField("Boundaries: ", GUILayout.Width(70));
            props.limit = EditorGUILayout.Toggle(props.limit, GUILayout.Width(20));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(30);
            EditorGUILayout.LabelField("Floor each ", GUILayout.Width(65));
            props.floorInterval = EditorGUILayout.IntField(props.floorInterval, GUILayout.Width(30));
            EditorGUILayout.LabelField("depths", GUILayout.Width(70));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(30);
            if (GUILayout.Button(Resources.Load<Texture2D>("Icons/save"), GUILayout.Width(25), GUILayout.Height(25)))
            {
                Utils.Save(path, props);
            }

            if (GUILayout.Button(Resources.Load<Texture2D>("Icons/load"), GUILayout.Width(25), GUILayout.Height(25)))
            {
                props = Utils.Load<MERoomProps>(path);
                MERoomProps.Instance = props;
            }
            EditorGUILayout.EndHorizontal();
        }

    }

    private void SnapResize()
    {
        props.dim.x = Mathf.Floor(props.dim.x / gridprops.size.x) * gridprops.size.x;
        props.dim.y = Mathf.Ceil(props.dim.y / gridprops.size.y) * gridprops.size.y;
        props.dim.z = Mathf.Round(props.dim.z / gridprops.size.x) * gridprops.size.x;
    }

    private void CheckBounds()
    {
        GameObject go = (Selection.activeGameObject) ? Selection.activeGameObject : FindObjectOfType(typeof(GameObject)) as GameObject;
        if (!go.name.Contains("Camera") || go.tag != "MainCamera")
        {
            go.transform.position = new Vector3(
                Mathf.Clamp(go.transform.position.x, 0, props.dim.x - gridprops.size.x),
                Mathf.Clamp(go.transform.position.y, gridprops.size.y, props.dim.y),
                Mathf.Clamp(go.transform.position.z, 0, props.dim.z)
            );
        }
    }

    public static bool CheckIfInside(MERoomProps props, MEGridProps gridprops, Vector2 point)
    {
        bool inside = false;
        if (point.x + .01f > 0 && point.x < props.dim.x)
        {
            if (point.y > 0 && point.y - .01f < props.dim.y)
            {
                inside = true;
            }
        }
        return inside;
    }

}