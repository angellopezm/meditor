﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;


[MenuTab(Section = "General")]
public class MEGrid: AbstractMEditorModule {

    private static readonly Lazy<MEGrid> lazy = new Lazy<MEGrid>(() => CreateInstance<MEGrid>());
    public MEGridProps props;
    public string path;

    public static MEGrid Instance { get { return lazy.Value; } }

    private MEGrid() {
    }

    private void OnEnable() {


        path = Application.dataPath + "/Editor/MEditor/" + GetType().Name + "/conf.json";

        if (File.Exists(path)) {
            MEGridProps.Instance = Utils.Load<MEGridProps>(path);
        }
        props = MEGridProps.Instance;

        SceneView.duringSceneGui -= OnSceneGUI;
        SceneView.duringSceneGui += OnSceneGUI;
    }

    override public void OnSceneGUI(SceneView sView) {

        if (props.enabled)
        {
            Render();
        }

        if (props.snap) {
            Snap();
        }
    }

    private void Render() {

        Handles.color = props.color;
        props.color.a = props.alpha;

        props.minGrid = SceneView.currentDrawingSceneView.camera.ScreenPointToRay(new Vector2(0f, 0f)).origin;
        props.maxGrid = SceneView.currentDrawingSceneView.camera.ScreenPointToRay(new Vector2(SceneView.currentDrawingSceneView.camera.pixelWidth, SceneView.currentDrawingSceneView.camera.pixelHeight)).origin;

        for (float i = Mathf.Round(props.minGrid.x / props.size.x) * props.size.x; i < Mathf.Round(props.maxGrid.x / props.size.x) * props.size.x && props.size.x > 0.05f; i += props.size.x) {
            Handles.DrawLine(new Vector3(i, props.minGrid.y, 0.0f), new Vector3(i, props.maxGrid.y, 0.0f));
        }
        for (float j = Mathf.Round(props.minGrid.y / props.size.y) * props.size.y; j < Mathf.Round(props.maxGrid.y / props.size.y) * props.size.y && props.size.y > 0.05f; j += props.size.y) {
            Handles.DrawLine(new Vector3(props.minGrid.x, j, 0.0f), new Vector3(props.maxGrid.x, j, 0.0f));
        }
    }

    override public void OnMenu() {

        EditorGUILayout.Space();
        props.expanded = EditorGUILayout.Foldout(props.expanded, "Grid");

        if (props.expanded) {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(30);
            EditorGUILayout.LabelField("Enable Grid ", GUILayout.Width(70));
            props.enabled = EditorGUILayout.Toggle(props.enabled, GUILayout.Width(20));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(30);
            props.size = EditorGUILayout.Vector2Field("Cell Size: ", props.size, GUILayout.Width(150));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(30);
            EditorGUILayout.LabelField("Alpha: ", GUILayout.Width(40));
            props.alpha = EditorGUILayout.FloatField(props.alpha, GUILayout.Width(50));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(30);
            EditorGUILayout.LabelField("Snap to grid ", GUILayout.Width(75));
            props.snap = EditorGUILayout.Toggle(props.snap);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(30);
            if (GUILayout.Button(Resources.Load<Texture2D>("Icons/save"), GUILayout.Width(25), GUILayout.Height(25))) {
                Utils.Save(path, props);
            }

            if (GUILayout.Button(Resources.Load<Texture2D>("Icons/load"), GUILayout.Width(25), GUILayout.Height(25))) {
                props = Utils.Load<MEGridProps>(path);
                METilePainter.Instance.gridprops = props;
            }
            EditorGUILayout.EndHorizontal();

        }

    }

    private void Snap() {
        GameObject go = (Selection.activeGameObject) ? Selection.activeGameObject : FindObjectOfType(typeof(GameObject)) as GameObject;

        if (!go.name.Contains("Camera") || !go.name.Contains("Main Camera")) {
            go.transform.position = new Vector3(
                Mathf.Round(go.transform.position.x * (1 / props.size.x)) / (1 / props.size.x),
                Mathf.Round(go.transform.position.y * (1 / props.size.y)) / (1 / props.size.y),
                Mathf.Round(go.transform.position.z * (1 / props.size.x)) / (1 / props.size.x)
            );
        }
    }

    private void CleanScene() {
        SceneView.RepaintAll();
    }
}