﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MEGridProps {

    private static MEGridProps instance = new MEGridProps();

    public static MEGridProps Instance { get { return instance; } set { instance = value; } }

    private MEGridProps() {
    }

    public Vector2 size = new Vector2(.32f, .32f);
    public Color color = Color.white;
    public float alpha = .03f;
    public bool expanded, enabled = true, snap = true;
    public Vector3 minGrid, maxGrid;

}
