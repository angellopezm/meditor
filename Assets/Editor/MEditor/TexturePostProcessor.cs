﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;
using System;

public class TexturePostProcessor : AssetPostprocessor
{

    MEGridProps gridprops = MEGridProps.Instance;
    METilesetLoaderProps props = METilesetLoaderProps.Instance;

    public void OnPostprocessTexture(Texture2D texture)
    {

        TextureImporter importer = null;
        List<SpriteMetaData> newData = null;
        SpriteMetaData smd;

        importer = assetImporter as TextureImporter;

        newData = new List<SpriteMetaData>();

        for (int i = 0; i <= texture.width; i += Mathf.FloorToInt(gridprops.size.x * 100))
        {
            for (int j = texture.height; j >= 1; j -= Mathf.FloorToInt(gridprops.size.y * 100))
            {

                smd = new SpriteMetaData
                {
                    name = "(" + i + "," + j + ")",
                    alignment = 1,
                    rect = new Rect(i, j - Mathf.CeilToInt(gridprops.size.y * 100), Mathf.CeilToInt(gridprops.size.x * 100), Mathf.CeilToInt(gridprops.size.y * 100))
                };

                newData.Add(smd);
            }
        }

        try
        {
            importer.textureType = TextureImporterType.Sprite;
            importer.spriteImportMode = SpriteImportMode.Multiple;
            importer.spritePivot = Vector2.zero;
            importer.spritePixelsPerUnit = 100;
            importer.spritePackingTag = "[TIGHT]";
            importer.sRGBTexture = true;
            importer.alphaSource = TextureImporterAlphaSource.FromInput;
            importer.alphaIsTransparency = true;
            importer.isReadable = true;
            importer.mipmapEnabled = false;
            importer.wrapMode = TextureWrapMode.Clamp;
            importer.filterMode = FilterMode.Point;
            importer.maxTextureSize = 4096;
            //importer.textureCompression = TextureImporterCompression.Uncompressed;

            importer.spritesheet = newData.ToArray();

        }
        catch (NullReferenceException) { }

        AssetDatabase.Refresh();

    }

}