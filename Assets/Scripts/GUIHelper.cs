﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;


public class GUIHelper : Editor
{
    public GUIStyle style = new GUIStyle();

    public static bool CreateButton(GUIContent content, params GUILayoutOption[] options)
    {
        bool reply = false;

        if (options != null) {
            reply = GUILayout.Button(content, options);
        }
        else
        {
            reply = GUILayout.Button(content);
        }

        return reply;
    }
    public static bool CreateButton(string content, params GUILayoutOption[] options)
    {
        bool reply = false;

        if (options != null)
        {
            reply = GUILayout.Button(content, options);
        }
        else
        {
            reply = GUILayout.Button(content);
        }

        return reply;
    }
    public static bool CreateButton(Texture2D content, params GUILayoutOption[] options)
    {
        bool reply = false;

        if (options != null)
        {
            reply = GUILayout.Button(content, options);
        }
        else
        {
            reply = GUILayout.Button(content);
        }

        return reply;
    }
    public static object CreateInput(string label, object data, [Optional] int width) {
        
        object reply = null;
        Type dataType = data.GetType();

        GUILayout.Label(label, GUILayout.Width(label.Length * 7.26f));
        if (dataType == typeof(string)) {
            reply = EditorGUILayout.TextField("", (string)data, GUILayout.Width(width), GUILayout.ExpandWidth(false));
        }
        else if (dataType == typeof(int)) {
            reply = EditorGUILayout.IntField("", (int)data, GUILayout.Width(width), GUILayout.ExpandWidth(false));
        }
        else if (dataType == typeof(float))
        {
            reply = EditorGUILayout.FloatField("", (float)data, GUILayout.Width(width), GUILayout.ExpandWidth(false));
        }

        return reply;
    }
    public static void Row(Action content)
    {
        GUILayout.BeginHorizontal();
        content.Invoke();
        GUILayout.EndHorizontal();
    }
    public static void Row(Action content, params GUILayoutOption[] options)
    {
        if (options != null)
        {
            GUILayout.BeginHorizontal(options);
            content.Invoke();
            GUILayout.EndHorizontal();
        }
        else
        {
            GUILayout.BeginHorizontal();
            content.Invoke();
            GUILayout.EndHorizontal();
        }
    }
    public static void Row(Action content, GUIStyle style)
    {
        if (style != null)
        {
            GUILayout.BeginHorizontal(style);
            content.Invoke();
            GUILayout.EndHorizontal();
        }
        else
        {
            GUILayout.BeginHorizontal();
            content.Invoke();
            GUILayout.EndHorizontal();
        }
    }
    public static void Row(Action content, GUIStyle style, params GUILayoutOption[] options) {
        if (options != null && style != null) {
            GUILayout.BeginHorizontal(style, options);
            content.Invoke();
            GUILayout.EndHorizontal();
        }
        else if (options != null)
        {
            GUILayout.BeginHorizontal(options);
            content.Invoke();
            GUILayout.EndHorizontal();
        }
        else if (style != null)
        {
            GUILayout.BeginHorizontal(style);
            content.Invoke();
            GUILayout.EndHorizontal();
        }
        else
        {
            GUILayout.BeginHorizontal();
            content.Invoke();
            GUILayout.EndHorizontal();
        }
    }
    public static void Column(Action content)
    {
        GUILayout.BeginVertical();
        content.Invoke();
        GUILayout.EndVertical();
    }
    public static void Column(Action content, params GUILayoutOption[] options)
    {
        if (options != null)
        {
            GUILayout.BeginVertical(options);
            content.Invoke();
            GUILayout.EndVertical();
        }
        else
        {
            GUILayout.BeginVertical();
            content.Invoke();
            GUILayout.EndVertical();
        }
    }
    public static void Column(Action content, GUIStyle style)
    {

        if (style != null)
        {
            GUILayout.BeginVertical(style);
            content.Invoke();
            GUILayout.EndVertical();
        }
        else
        {
            GUILayout.BeginVertical();
            content.Invoke();
            GUILayout.EndVertical();
        }
    }
    public static void Column(Action content, GUIStyle style, params GUILayoutOption[] options)
    {
        if (options != null && style != null)
        {
            GUILayout.BeginVertical(style, options);
            content.Invoke();
            GUILayout.EndVertical();
        }
        else if (options != null)
        {
            GUILayout.BeginVertical(options);
            content.Invoke();
            GUILayout.EndVertical();
        }
        else if (style != null)
        {
            GUILayout.BeginVertical(style);
            content.Invoke();
            GUILayout.EndVertical();
        }
        else
        {
            GUILayout.BeginVertical();
            content.Invoke();
            GUILayout.EndVertical();
        }
    }

}

public class StyleHelper {

    public GUIStyle style;

    public StyleHelper() {
        style = new GUIStyle();
    }
    public StyleHelper CenterText()
    {
        style.alignment = TextAnchor.MiddleCenter;
        return this;
    }
    public StyleHelper BoldText()
    {
        style.fontStyle = FontStyle.Bold;
        return this;
    }
    public StyleHelper BoxBackground() {
        style = EditorStyles.helpBox;
        return this;
    }

    public StyleHelper FontColor(Color color)
    {
        style.normal.textColor = color;
        return this;
    }
}

public class PopupWindow : EditorWindow
{
    private static PopupWindow instance;
    private bool focused;
    private Action<PopupWindow> innerGUI;

    public PopupWindow Instance { get => instance; set => instance = value; }

    public void Init(string name, Vector2 size, Action<PopupWindow> content)
    {
        if (Instance != null)
        {
            Instance.Close();
        }

        Instance = CreateInstance<PopupWindow>();
        Instance.titleContent = new GUIContent(name);
        var sceneViewInst = SceneView.GetWindow<SceneView>();
        var position = Instance.position;

        position.size = new Vector2(size.x, size.y);
        position.center = new Vector2(sceneViewInst.position.width*.78f - 2, sceneViewInst.position.height*.7f - 36);
        Instance.position = position;

        Instance.innerGUI = content;
        Instance.ShowPopup();
    }

    private void OnGUI()
    {
        GUILayout.Space(5);
        GUIHelper.Row(() => {
            if (GUIHelper.CreateButton("X", GUILayout.Width(20)))
            {
                Close();
            }
            GUILayout.Label(Instance.titleContent, new StyleHelper().CenterText().BoldText().style, GUILayout.ExpandWidth(true));
        }, EditorStyles.helpBox);
        GUILayout.Space(5);
        innerGUI.Invoke(Instance);
    }

}