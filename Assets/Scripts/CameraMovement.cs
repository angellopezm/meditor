﻿using UnityEngine;

public class CameraMovement : MonoBehaviour {

    Vector3 mov;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        mov = new Vector3(Mathf.CeilToInt(Input.GetAxis("Horizontal")), Input.GetAxis("Vertical"), 0);
        mov.Normalize();
        Vector3.ClampMagnitude(mov, 1);

        transform.position += mov * 2f * Time.deltaTime;
        
	}
}
